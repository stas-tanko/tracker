FROM mhart/alpine-node:10
WORKDIR /usr/src
COPY package.json yarn.lock ./
RUN yarn
COPY ip-data/geoip-city-names.dat ./node_modules/geoip-lite-country/data
COPY ip-data/geoip-city.dat ./node_modules/geoip-lite-country/data
COPY . .
RUN yarn run assets:build
EXPOSE 8080
CMD ["yarn", "run", "start"]
