const path = require('path');
const SaveHashes = require('assets-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

// rules

const javascript = {
  test: /\.(js)$/,
  exclude: /node_modules/,
  use: {
    loader: 'babel-loader',
    options: {
      presets: [
        '@babel/env',
        '@babel/react'
      ],
      plugins: [
        '@babel/plugin-proposal-class-properties',
        ['@babel/plugin-proposal-decorators', { 'legacy': true }]
      ]
    }
  }
};

const styles = {
  test: /\.(scss)$/,
  use: [
    MiniCssExtractPlugin.loader,
    'css-loader',
    'sass-loader'
  ]
};

// plugins

const saveHashes = new SaveHashes({
  path: path.join(__dirname, '../assets')
});

const miniCssExtractPlugin = new MiniCssExtractPlugin({
  filename: 'main-[chunkhash].css'
});

// config

module.exports = {
  entry: [
    './client/index.js',
    './client/styles/index.scss'
  ],
  output: {
    path: path.resolve(__dirname, '../assets/dist'),
    filename: 'main-[chunkhash].js',
    publicPath: '/dist/'
  },
  module: {
    rules: [
      javascript,
      styles
    ]
  },
  plugins: [
    saveHashes,
    miniCssExtractPlugin
  ]
};
