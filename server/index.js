const express = require('express');
const path = require('path');
const useragent = require('express-useragent');
const bodyParser = require('body-parser');

const assets = require('./middleware/assets');
const routes = require('./routes');

require('dotenv').config();
require('./models');

const app = express();

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));
app.use(assets);
app.use(useragent.express());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/', routes);

app.listen(8080);
