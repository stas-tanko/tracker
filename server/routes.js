const express = require('express');
const router  = express.Router();

const landers = require('./controllers/landers');
const campaigns = require('./controllers/campaigns');
const clicks = require('./controllers/clicks');

router.post('/api/landers', landers.create);
router.get('/api/landers', landers.list);
router.patch('/api/landers/:id', landers.update);
router.delete('/api/landers/:id', landers.destroy);

router.post('/api/campaigns', campaigns.create);
router.get('/api/campaigns', campaigns.list);
router.patch('/api/campaigns/:id', campaigns.update);
router.delete('/api/campaigns/:id', campaigns.destroy);

router.get('/api/clicks', clicks.list);
router.get('/click/:id', clicks.handle);

router.get('*', (req, res) => res.render('index'));

module.exports = router;
