const { compose } = require('compose-middleware');
const express = require('express');
const webpackAssets = require('express-webpack-assets');

const devMode = process.env.NODE_ENV !== 'production';

const middlwares = [
  express.static('assets'),
  webpackAssets('./assets/webpack-assets.json', { devMode })
];

if (devMode) {
  const webpack = require('webpack');
  const webpackDevMiddleware = require('webpack-dev-middleware');
  const config = require('../../webpack-config/dev');
  const compiler = webpack(config);

  middlwares.push(webpackDevMiddleware(compiler, { publicPath: '/dist/' }));
};

module.exports = compose(middlwares);
