const Campaign = require('../models/campaign');
const { serialize } = require('../serializers/campaign');

module.exports.create = async (req, res) => {
  const campaign = await new Campaign(req.body);
  await campaign.save();
  await campaign.populate('lander');
  const withLander = await Campaign.findOne({ _id: campaign._id }).populate('lander');
  res.json(serialize(withLander));
};

module.exports.list = async (req, res) => {
  const campaigns = await Campaign.find().populate('lander');
  res.json(campaigns.map(serialize));
};

module.exports.update = async (req, res) => {
  const campaign = await Campaign.findOneAndUpdate(
    { _id: req.params.id },
    req.body,
    { new: true }
  ).exec();
  const withLander = await Campaign.findOne({ _id: campaign._id }).populate('lander');
  res.json(serialize(withLander));
};

module.exports.destroy = async (req, res) => {
  await Campaign.deleteOne({ _id: req.params.id });
  res.json({});
};
