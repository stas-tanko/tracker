const Lander = require('../models/lander');

module.exports.create = async (req, res) => {
  const lander = new Lander(req.body);
  await lander.save();
  res.json(lander);
};

module.exports.list = async (req, res) => {
  const landers = await Lander.find({}, { name: 1, url: 1 });
  res.json(landers);
};

module.exports.update = async (req, res) => {
  const lander = await Lander.findOneAndUpdate(
    { _id: req.params.id },
    req.body,
    { new: true }
  ).exec();
  res.json(lander);
};

module.exports.destroy = async (req, res) => {
  await Lander.deleteOne({ _id: req.params.id });
  res.json({});
};
