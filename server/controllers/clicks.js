const Campaign = require('../models/campaign');
const Click = require('../models/click');
const geoip = require('geoip-lite-country');

module.exports.handle = async (req, res) => {
  const campaign = await Campaign.findOne({ _id: req.params.id }).populate('lander');
  const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
  const clickData = {
    time:     new Date(),
    campaign: campaign.name,
    lander:   campaign.lander.name,
    country:  (geoip.lookup(ip) || {}).country,
    ip:       ip,
    os:       req.useragent.os,
    browser:  req.useragent.browser
  };
  const click = await new Click(clickData);
  await click.save();
  res.redirect(campaign.lander.url);
};

module.exports.list = async (req, res) => {
  const clicks = await Click.find({}, {}, { sort: { time: -1 } });
  res.json(clicks);
}
