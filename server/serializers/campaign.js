module.exports.serialize = (campaign) => ({
  _id:        campaign._id,
  name:       campaign.name,
  landerId:   campaign.lander._id,
  landerName: campaign.lander.name,
  clickUrl:   `/click/${campaign._id}`
});
