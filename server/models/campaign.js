const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const schema = new mongoose.Schema({
  name: {
    type: String
  },

  lander: {
    type:     mongoose.Schema.ObjectId,
    ref:      'Lander',
    required: true
  }
});

schema.index({
  name: 1,
});

module.exports = mongoose.model('Campaign', schema);
