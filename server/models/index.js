const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

mongoose.connect(process.env.DATABASE, { useNewUrlParser: true });

mongoose.connection.on(
  'error',
  err => console.error(`Mongoose error > ${err.message}`)
);

require('./lander');
require('./campaign');
require('./click');
