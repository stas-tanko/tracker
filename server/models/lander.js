const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const schema = new mongoose.Schema({
  name: {
    type: String
  },

  url: {
    type: String
  }
});

schema.index({
  name: 1,
});

module.exports = mongoose.model('Lander', schema);
