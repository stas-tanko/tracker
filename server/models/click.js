const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const schema = new mongoose.Schema({
  time: {
    type: Date
  },
  campaign: {
    type: String
  },
  lander: {
    type: String
  },
  country: {
    type: String
  },
  ip: {
    type: String
  },
  os: {
    type: String
  },
  browser: {
    type: String
  }
});

schema.index({
  name: 1,
});

module.exports = mongoose.model('Click', schema);
