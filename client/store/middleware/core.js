import { applyMiddleware, compose } from 'redux';
import { createBrowserHistory } from 'history';

import { routerMiddleware as createRouterMiddleware } from 'react-router-redux';
import { createLogger } from 'redux-logger';
import createSagaMiddleware from 'redux-saga';

const devMode = process.env.NODE_ENV !== 'production';

const logger = createLogger({
  duration:  true,
  collapsed: true
});

const history = createBrowserHistory();
const routerMiddlewate = createRouterMiddleware(history);
const sagaMiddleware = createSagaMiddleware();
const devtools = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__;
const composeEnhancers = devMode && devtools ? devtools : compose;

const middleware = [sagaMiddleware, routerMiddlewate];

if (devMode) {
  middleware.push(logger);
}

const enhancedStore = composeEnhancers(applyMiddleware(...middleware));

export { enhancedStore, sagaMiddleware, history };
