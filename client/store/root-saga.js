import { all, call } from 'redux-saga/effects';

import { watchLanders } from '../bus/landers/saga/watchers';
import { watchCampaigns } from '../bus/campaigns/saga/watchers';
import { watchClicks } from '../bus/clicks/saga/watchers';

export function* rootSaga() {
  yield all([
    call(watchLanders),
    call(watchCampaigns),
    call(watchClicks)
  ]);
}
