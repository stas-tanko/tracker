import { combineReducers } from 'redux';
import { routerReducer as router } from 'react-router-redux';

import { landersReducer as landers } from '../bus/landers/reducer';
import { campaignsReducer as campaigns } from '../bus/campaigns/reducer';
import { clicksReducer as clicks } from '../bus/clicks/reducer';

export const rootReducer = combineReducers({
  router,
  landers,
  campaigns,
  clicks
});
