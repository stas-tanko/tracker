import { createStore } from 'redux';

import { rootReducer } from './root-reducer';
import { rootSaga } from './root-saga';
import { enhancedStore, sagaMiddleware } from './middleware/core';

export const store = createStore(rootReducer, enhancedStore);

sagaMiddleware.run(rootSaga);
