import React from 'react';
import cx from 'classnames';

import { asFormInput } from './as-form-input';

const TextInputRaw = ({
  label,
  type = 'text',
  value = '',
  error,
  className,
  ...props
}) => (
  <input
    type={type}
    className={cx(
      className,
      'uk-input',
      error && 'uk-form-danger'
    )}
    value={value}
    {...props}
  />
);

export const TextInput = asFormInput(TextInputRaw);
