import React, { Component } from 'react';
import UIkit from 'uikit';

const modalOptions = {
  bgClose: false,
  selClose: null,
  escClose: false
};

export class Modal extends Component {

  static defaultProps = {
    onClose: () => {}
  };

  modal = null;

  handleRef = (node) => {
    if (node) {
      this.modal = UIkit.modal(node, modalOptions);
    } else {
      this.modal && this.modal.$destroy();
      this.modal = null;
    }
  };

  componentDidMount() {
    if (this.props.open) this.modal.show();
  }

  componentDidUpdate(prevProps) {
    if (!prevProps.open && this.props.open) this.modal.show();
    if (prevProps.open && !this.props.open) this.modal.hide();
  }

  handleClick = ({ target }) => {
    const { onClose } = this.props;
    if (target.classList.contains('uk-modal')) onClose();
    if (target.classList.contains('uk-modal-close-default')) onClose();
    if (target.nodeName === 'svg' && target.parentElement.classList.contains('uk-modal-close-default')) onClose();
  };

  render() {
    const { title, children } = this.props;

    return (
      <div>
        <div ref={this.handleRef} onClick={this.handleClick}>
          <div className='uk-modal-dialog uk-modal-body'>
            <button
              className='uk-modal-close-default uk-close uk-icon'
              type='button'
              data-uk-close
            />
            <h3 className='uk-modal-title'>{title}</h3>
            {children}
          </div>
        </div>
      </div>
    );
  }

}
