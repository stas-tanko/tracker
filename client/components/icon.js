import React from 'react';
import cx from 'classnames';

export const Icon = ({ name, className, ratio = 1, ...props }) => (
  <a
    data-uk-icon={`icon: ${name}; ratio: ${ratio}`}
    className={cx(className, 'uk-icon-link')}
    {...props}
  />
);
