import React from 'react';

export const asFormInput = (Component) => (props) => {
  const { id, label, error } = props;

  return (
    <div className='uk-margin'>
      <label className='uk-form-label' htmlFor={id}>{label}</label>
      <div className='uk-form-controls'>
        <Component {...props} />
        {error ? <small className='uk-form-danger'>{error}</small> : null}
      </div>
    </div>
  );
}
