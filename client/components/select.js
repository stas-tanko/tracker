import React from 'react';
import cx from 'classnames';
import sortBy from 'lodash/sortBy';

import { asFormInput } from './as-form-input';

const blankOption = {
  value: '', label: 'Please select'
};

const _renderOption = (option) => (
  <option value={option.value} key={option.value}>
    {option.label}
  </option>
);

const SelectRaw = ({
  label,
  value = '',
  error,
  className,
  options,
  renderOption = _renderOption,
  ...props
}) => (
  <select
    className={cx(
      className,
      'uk-select',
      error && 'uk-form-danger'
    )}
    value={value}
    {...props}
  >
    {renderOption(blankOption)}
    {sortBy(options, 'label').map(renderOption)}
  </select>
);

export const Select = asFormInput(SelectRaw);
