import React from 'react';

import { Row } from './row';

const renderHeader = ({ key, content }) => (
  <th key={key}>
    {content}
  </th>
);

export const Table = ({ headers, children }) => (
  <div>
    <table className="uk-table uk-table-divider uk-table-striped uk-table-small">
      <thead>
        <tr>{headers.map(renderHeader)}</tr>
      </thead>
      <tbody style={{ borderBottom: '1px solid #e5e5e5' }}>
        {children}
      </tbody>
    </table>
  </div>
);

Table.Row = Row;
