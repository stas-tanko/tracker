import React from 'react';

const renderCell = ({ key, content }) => (
  <td key={key}>
    {content}
  </td>
);

export const Row = ({ children }) => (
  <tr>
    {children.map(renderCell)}
  </tr>
);
