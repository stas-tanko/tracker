import React from 'react';
import cx from 'classnames';

export const Button = ({ color, ...props }) => (
  <button
    className={cx(
      'uk-button',
      'uk-button-small',
      `uk-button-${color}`
    )}
    {...props}
  />
);

Button.defaultProps = {
  color: 'default',
  type:  'button'
};
