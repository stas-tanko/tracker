import React from 'react';

import "./styles.scss";

export const PageWrapper = ({ children, classes }) => (
  <div id='page-wrapper'>
    {children}
  </div>
);
