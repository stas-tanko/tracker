import React, { Fragment } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import { Navbar } from './navbar';
import { PageTitle } from './page-title';
import { PageWrapper } from './page-wrapper';

import { Campaigns, Landers, Clicklog } from '../pages';
import { routes } from '../routes';

export const App = () => (
  <Fragment>
    <Navbar />
    <PageTitle />
    <PageWrapper>
      <Switch>
        <Route component={Campaigns} path={routes.campaigns.path} />
        <Route component={Landers} path={routes.landers.path} />
        <Route component={Clicklog} path={routes.clicklog.path} />
        <Redirect to={routes.campaigns.path} />
      </Switch>
    </PageWrapper>
  </Fragment>
);
