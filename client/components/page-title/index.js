import React, { Component } from 'react';
import { connect } from 'react-redux';

import { routes } from '../../routes';

import "./styles.scss";

const mapState = (state) => ({
  location: state.router.location
});

@connect(mapState)
export class PageTitle extends Component {

  get route() {
    const { pathname } = this.props.location;
    return Object.values(routes).find(r => r.path === pathname) || {};
  }

  setDocumentTitle = () => {
    document.title = [this.route.title, "Tracker"].filter(Boolean).join(" • ");
  };

  componentDidMount() {
    this.setDocumentTitle();
  }

  componentDidUpdate() {
    this.setDocumentTitle();
  }

  render() {
    return (
      <h3 id='page-title'>
        {this.route.title}
      </h3>
    );
  }

}
