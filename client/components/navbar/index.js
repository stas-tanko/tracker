import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import cx from 'classnames';

import { routes } from '../../routes';

import "./styles.scss";

const mapStateToProps = (state) => ({
  location: state.router.location
});

@connect(mapStateToProps)
export class Navbar extends Component {

  renderLink = (key) => {
    const { path, title } = routes[key];
    const { location } = this.props;

    return (
      <li
        key={title}
        className={cx(location.pathname === path && 'uk-active')}
      >
        <Link to={path}>{title}</Link>
      </li>
    );
  }

  render() {
    return (
      <nav
        className='uk-navbar-container uk-margin uk-navbar'
        data-uk-navbar
        style={{ marginBottom: 0 }}
      >
        <div className='uk-navbar-left'>
          <ul className='uk-navbar-nav'>
            {Object.keys(routes).map(this.renderLink)}
          </ul>
        </div>
      </nav>
    );
  }

}
