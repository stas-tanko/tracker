import axios from 'axios';

export const campaignsApi = {
  create: (payload) => (
    axios.post('/api/campaigns', payload)
  ),

  update: (payload) => (
    axios.patch(`/api/campaigns/${payload._id}`, payload)
  ),

  fetch: () => (
    axios.get('/api/campaigns')
  ),

  delete: (id) => (
    axios.delete(`/api/campaigns/${id}`)
  )
};
