import axios from 'axios';

export const clicksApi = {
  fetch: () => (
    axios.get('/api/clicks')
  )
};
