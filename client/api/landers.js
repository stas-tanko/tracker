import axios from 'axios';

export const landersApi = {
  create: (payload) => (
    axios.post('/api/landers', payload)
  ),

  update: (payload) => (
    axios.patch(`/api/landers/${payload._id}`, payload)
  ),

  fetch: () => (
    axios.get('/api/landers')
  ),

  delete: (id) => (
    axios.delete(`/api/landers/${id}`)
  )
};
