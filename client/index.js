import 'babel-polyfill';

import React from 'react';
import { Provider } from 'react-redux';
import { ConnectedRouter as Router } from 'react-router-redux';
import { render } from 'react-dom';

import UIkit from 'uikit';
import Icons from 'uikit/dist/js/uikit-icons';

import { store } from './store';
import { history } from './store/middleware/core';

import { App } from './components/app';

UIkit.use(Icons);

render(
  <Provider store={store}>
    <Router history={history}>
      <App />
    </Router>
  </Provider>,
  document.querySelector('#react-app')
);
