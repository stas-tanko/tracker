import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import sortBy from 'lodash/sortBy';

import { Button } from '../../components/button';

import { CampaignModal } from './modal';
import { CampaignsTable } from './table';

import { campaignsActions } from '../../bus/campaigns/actions';
import { landersActions } from '../../bus/landers/actions';

const mapState = (state) => ({
  campaigns: state.campaigns.items,
  landers:   state.landers.items
});

const mapDispatch = {
  ...campaignsActions,
  fetchLandersAsync: landersActions.fetchLandersAsync
};

@connect(mapState, mapDispatch)
export class Campaigns extends Component {

  state = {
    isModalOpen: false,
    form: {}
  };

  componentDidMount() {
    this.props.fetchCampaignsAsync();
    this.props.fetchLandersAsync();
  }

  handleAddClick = () => this.setState({ isModalOpen: true, form: {} });

  hideModal = () => this.setState({ isModalOpen: false });

  handleEditClick = ({ _id, name, landerId }) => () => {
    const form = { _id, name, lander: landerId };
    this.setState({ form, isModalOpen: true });
  };

  handleDeleteClick = ({ _id, name }) => () => {
    const message = `Are you sure you would like to delete '${name}'?`;
    if (!confirm(message)) return;
    this.props.deleteCampaignAsync(_id);
  };

  render() {
    const { campaigns, landers, saveCampaignAsync } = this.props;

    return (
      <Fragment>
        <div className="uk-flex uk-flex-right">
          <Button color='primary' onClick={this.handleAddClick}>
            Add Campaign
          </Button>
        </div>

        <CampaignsTable
          campaigns={sortBy(campaigns, 'name')}
          onEditClick={this.handleEditClick}
          onDeleteClick={this.handleDeleteClick}
        />

        <CampaignModal
          open={this.state.isModalOpen}
          onClose={this.hideModal}
          form={this.state.form}
          saveCampaignAsync={saveCampaignAsync}
          landers={landers}
        />
      </Fragment>
    );
  }

}
