import React, { Component } from 'react';
import { withFormik } from 'formik';
import * as yup from 'yup';

import { Modal, TextInput, Select, Button } from '../../components';

const formikEnhancer = withFormik({
  mapPropsToValues: ({ form }) => form,

  enableReinitialize: true,

  handleSubmit: (payload, { props, resetForm, setSubmitting }) => {
    props.saveCampaignAsync(payload, {
      onSuccess: () => {
        props.onClose();
        resetForm();
      }
    });
    setSubmitting(false);
  },

  validationSchema: yup.object().shape({
    name: yup.string().required('is required'),
    lander: yup.string().required('is required')
  })
});

const makeLanderOption = ({ _id, name }) => ({
  value: _id,
  label: name
});

@formikEnhancer
export class CampaignModal extends Component {

  render() {
    const {
      open,
      onClose,
      values,
      handleSubmit,
      handleChange,
      handleBlur,
      touched,
      errors,
      landers
    } = this.props;

    return (
      <Modal
        open={open}
        onClose={onClose}
        title="Campaign"
      >
        <form onSubmit={handleSubmit}>
          <TextInput
            id='name'
            label='Name'
            value={values.name}
            onChange={handleChange}
            onBlur={handleBlur}
            error={touched.name && errors.name}
          />

          <Select
            id='lander'
            label='Lander'
            options={landers.map(makeLanderOption)}
            value={values.lander}
            onChange={handleChange}
            onBlur={handleBlur}
            error={touched.lander && errors.lander}
          />

          <Button type='submit' color='primary'>Save</Button>
        </form>
      </Modal>
    );
  }

}
