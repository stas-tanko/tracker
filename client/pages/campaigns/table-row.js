import React from 'react';

import { Table, Icon } from '../../components';

export const CampaignRow = ({ name, clickUrl, landerName, onEditClick, onDeleteClick }) => (
  <Table.Row>
    {[
      { key: 'name', content: name },
      { key: 'lander', content: landerName },
      { key: 'url', content: `${window.location.origin}${clickUrl}` },
      {
        key: 'actions',
        content: (
          <span>
            <Icon name='file-edit' onClick={onEditClick} />
            &nbsp;&nbsp;
            <Icon name='trash' onClick={onDeleteClick} />
          </span>
        )
      }
    ]}
  </Table.Row>
);
