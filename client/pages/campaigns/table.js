import React from 'react';

import { Table } from '../../components';
import { CampaignRow } from './table-row';

const headers = [
  { key: 'name', content: 'Name' },
  { key: 'lander', content: 'Lander' },
  { key: 'url', content: 'Click URL' },
  { key: 'actions', content: '' }
];

const renderRow = (onEditClick, onDeleteClick) => (campaign) => (
  <CampaignRow
    key={campaign._id}
    onEditClick={onEditClick(campaign)}
    onDeleteClick={onDeleteClick(campaign)}
    {...campaign}
  />
);

export const CampaignsTable = ({ campaigns, onEditClick, onDeleteClick }) => {
  if (campaigns.length === 0) {
    return <p>No campaigns have been added yet</p>;
  }

  return (
    <Table headers={headers}>
      {campaigns.map(renderRow(onEditClick, onDeleteClick))}
    </Table>
  );
}
