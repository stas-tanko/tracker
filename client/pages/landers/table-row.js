import React from 'react';

import { Table, Icon } from '../../components';

export const LanderRow = ({ name, url, onEditClick, onDeleteClick }) => (
  <Table.Row>
    {[
      { key: 'name', content: name },
      { key: 'url', content: url },
      {
        key: 'actions',
        content: (
          <span>
            <Icon name='file-edit' onClick={onEditClick} />
            &nbsp;&nbsp;
            <Icon name='trash' onClick={onDeleteClick} />
          </span>
        )
      }
    ]}
  </Table.Row>
);
