import React, { Component } from 'react';
import { withFormik } from 'formik';
import * as yup from 'yup';

import { Modal, TextInput, Button } from '../../components';

const formikEnhancer = withFormik({
  mapPropsToValues: ({ form }) => form,

  enableReinitialize: true,

  handleSubmit: (payload, { props, resetForm, setSubmitting }) => {
    props.saveLanderAsync(payload, {
      onSuccess: () => {
        props.onClose();
        resetForm();
      }
    });
    setSubmitting(false);
  },

  validationSchema: yup.object().shape({
    name: yup.string().required('is required'),
    url: yup.string().required('is required').url('is not a valid URL')
  })
});

@formikEnhancer
export class LanderModal extends Component {

  render() {
    const {
      open,
      onClose,
      values,
      handleSubmit,
      handleChange,
      handleBlur,
      touched,
      errors
    } = this.props;

    return (
      <Modal
        open={open}
        onClose={onClose}
        title="New Lander"
      >
        <form onSubmit={handleSubmit}>
          <TextInput
            id='name'
            label='Name'
            value={values.name}
            onChange={handleChange}
            onBlur={handleBlur}
            error={touched.name && errors.name}
          />

          <TextInput
            id='url'
            label='URL'
            value={values.url}
            onChange={handleChange}
            onBlur={handleBlur}
            error={touched.url && errors.url}
          />

          <Button type='submit' color='primary'>Save</Button>
        </form>
      </Modal>
    );
  }

}
