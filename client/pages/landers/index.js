import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import sortBy from 'lodash/sortBy';

import { Button } from '../../components/button';

import { LanderModal } from './modal';
import { LandersTable } from './table';

import { landersActions } from '../../bus/landers/actions';

const mapState = (state) => ({
  landers: state.landers.items
});

@connect(mapState, landersActions)
export class Landers extends Component {

  state = {
    isModalOpen: false,
    form: {}
  };

  componentDidMount() {
    this.props.fetchLandersAsync();
  }

  handleAddClick = () => this.setState({ isModalOpen: true, form: {} });

  hideModal = () => this.setState({ isModalOpen: false });

  handleEditClick = (lander) => () => this.setState({ form: lander, isModalOpen: true });

  handleDeleteClick = ({ _id, name }) => () => {
    const message = `Are you sure you would like to delete '${name}'?`;
    if (!confirm(message)) return;
    this.props.deleteLanderAsync(_id);
  };

  render() {
    const { landers, saveLanderAsync } = this.props;
 
    return (
      <Fragment>
        <div className="uk-flex uk-flex-right">
          <Button color='primary' onClick={this.handleAddClick}>
            Add Lander
          </Button>
        </div>

        <LandersTable
          landers={sortBy(landers, 'name')}
          onEditClick={this.handleEditClick}
          onDeleteClick={this.handleDeleteClick}
        />

        <LanderModal
          open={this.state.isModalOpen}
          onClose={this.hideModal}
          saveLanderAsync={saveLanderAsync}
          form={this.state.form}
        />
      </Fragment>
    );
  }

};
