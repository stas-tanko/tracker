import React from 'react';

import { Table } from '../../components';
import { LanderRow } from './table-row';

const headers = [
  { key: 'name', content: 'Name' },
  { key: 'url', content: 'URL' },
  { key: 'actions', content: '' }
];

const renderRow = (onEditClick, onDeleteClick) => (lander) => (
  <LanderRow
    key={lander._id}
    onEditClick={onEditClick(lander)}
    onDeleteClick={onDeleteClick(lander)}
    {...lander}
  />
);

export const LandersTable = ({ landers, onEditClick, onDeleteClick }) => {
  if (landers.length === 0) {
    return <p>No landers have been added yet</p>;
  }

  return (
    <Table headers={headers}>
      {landers.map(renderRow(onEditClick, onDeleteClick))}
    </Table>
  );
}
