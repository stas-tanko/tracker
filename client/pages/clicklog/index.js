import React, { Component } from 'react';
import { connect } from 'react-redux';

import { clicksActions } from '../../bus/clicks/actions';

import { ClicksTable } from './table'

const mapState = (state) => ({
  clicks: state.clicks.items
});

@connect(mapState, clicksActions)
export class Clicklog extends Component {

  componentDidMount() {
    this.props.fetchClicksAsync();
  }

  render() {
    const { clicks } = this.props;

    return (
      <ClicksTable clicks={clicks} />
    );
  }

}
