import React from 'react';

import { Table, Icon } from '../../components';

export const ClickRow = ({ _id, time, campaign, lander, country, ip, os, browser }) => (
  <Table.Row>
    {[
      { key: 'id', content: _id },
      { key: 'time', content: new Date(time).toLocaleString() },
      { key: 'campaign', content: campaign },
      { key: 'lander', content: lander },
      { key: 'country', content: country },
      { key: 'ip', content: ip },
      { key: 'os', content: os },
      { key: 'browser', content: browser }
    ]}
  </Table.Row>
);
