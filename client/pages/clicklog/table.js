import React from 'react';

import { Table } from '../../components';
import { ClickRow } from './table-row';

const headers = [
  { key: 'id', content: 'Click ID' },
  { key: 'time', content: 'Click Time' },
  { key: 'campaign', content: 'Campaign' },
  { key: 'lander', content: 'Lander' },
  { key: 'country', content: 'Country' },
  { key: 'ip', content: 'IP Address' },
  { key: 'os', content: 'OS' },
  { key: 'browser', content: 'Browser' }
];

const renderRow = (click) => (
  <ClickRow
    key={click._id}
    {...click}
  />
);

export const ClicksTable = ({ clicks }) => {
  if (clicks.length === 0) {
    return <p>No clicks tracked yet</p>;
  }

  return (
    <Table headers={headers}>
      {clicks.map(renderRow)}
    </Table>
  );
}
