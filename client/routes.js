export const routes = {
  campaigns: {
    path: '/campaigns',
    title: 'Campaigns'
  },
  landers: {
    path: '/landers',
    title: 'Landers'
  },
  clicklog: {
    path: '/clicklog',
    title: 'Clicklog'
  }
};
