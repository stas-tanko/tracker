import { types } from './types';

const initialState = {
  items: [],
};

export const clicksReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case types.FETCH_CLICKS:
      return { items: payload };

    default:
      return state;
  }
};
