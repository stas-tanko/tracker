import { takeEvery, all, call } from 'redux-saga/effects';

import { types } from '../types';
import { fetchClicks } from './workers';

function* watchFetchClicks() {
  yield takeEvery(types.FETCH_CLICKS_ASYNC, fetchClicks);
}

export function* watchClicks() {
  yield all([
    call(watchFetchClicks)
  ]);
}
