import { apply, put } from 'redux-saga/effects';

import { clicksApi } from '../../../../api/clicks';
import { clicksActions } from '../../../clicks/actions';

export function* fetchClicks() {
  try {
    const { data } = yield apply(clicksApi, clicksApi.fetch);
    yield put(clicksActions.fetchClicks(data));
  } catch (error) {
    console.log(error);
  }
};
