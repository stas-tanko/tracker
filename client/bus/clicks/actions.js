import { types } from './types';

export const clicksActions = {
  fetchClicks: (payload) => ({
    type: types.FETCH_CLICKS,
    payload
  }),

  fetchClicksAsync: () => ({
    type: types.FETCH_CLICKS_ASYNC
  })
};
