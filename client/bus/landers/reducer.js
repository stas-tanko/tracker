import { types } from './types';

const initialState = {
  items: [],
};

export const landersReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case types.CREATE_LANDER:
      return { items: [...state.items, payload] };

    case types.UPDATE_LANDER:
      return {
        items: [
          ...state.items.filter((item) => item._id !== payload._id),
          payload
        ]
      };

    case types.FETCH_LANDERS:
      return { items: payload };

    case types.DELETE_LANDER:
      return {
        items: state.items.filter((item) => item._id !== payload)
      };

    default:
      return state;
  }
};
