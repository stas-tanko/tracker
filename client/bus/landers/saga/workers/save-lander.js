import { apply, put } from 'redux-saga/effects';

import { landersApi } from '../../../../api/landers';
import { landersActions } from '../../../landers/actions';

export function* saveLander({ payload, meta }) {
  try {
    if (payload._id) {
      const { data } = yield apply(landersApi, landersApi.update, [payload]);
      yield put(landersActions.updateLander(data));
    } else {
      const { data } = yield apply(landersApi, landersApi.create, [payload]);
      yield put(landersActions.createLander(data));
    }
    yield apply(meta, meta.onSuccess);
  } catch (error) {
    console.log(error);
  }
};
