import { apply, put } from 'redux-saga/effects';

import { landersApi } from '../../../../api/landers';
import { landersActions } from '../../../landers/actions';

export function* deleteLander({ payload: id }) {
  try {
    yield apply(landersApi, landersApi.delete, [id]);
    yield put(landersActions.deleteLander(id));
  } catch (error) {
    console.log(error);
  }
};
