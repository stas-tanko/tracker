import { apply, put } from 'redux-saga/effects';

import { landersApi } from '../../../../api/landers';
import { landersActions } from '../../../landers/actions';

export function* fetchLanders() {
  try {
    const { data } = yield apply(landersApi, landersApi.fetch);
    yield put(landersActions.fetchLanders(data));
  } catch (error) {
    console.log(error);
  }
};
