import { takeEvery, all, call } from 'redux-saga/effects';

import { types } from '../types';
import { saveLander, fetchLanders, deleteLander } from './workers';

function* watchSaveLander() {
  yield takeEvery(types.SAVE_LANDER_ASYNC, saveLander);
}

function* watchFetchLanders() {
  yield takeEvery(types.FETCH_LANDERS_ASYNC, fetchLanders);
}

function* watchDeleteLander() {
  yield takeEvery(types.DELETE_LANDER_ASYNC, deleteLander);
}

export function* watchLanders() {
  yield all([
    call(watchSaveLander),
    call(watchFetchLanders),
    call(watchDeleteLander)
  ]);
}
