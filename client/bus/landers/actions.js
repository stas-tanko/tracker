import { types } from './types';

export const landersActions = {
  fetchLanders: (payload) => ({
    type: types.FETCH_LANDERS,
    payload
  }),

  createLander: (payload) => ({
    type: types.CREATE_LANDER,
    payload
  }),

  updateLander: (payload) => ({
    type: types.UPDATE_LANDER,
    payload
  }),

  deleteLander: (payload) => ({
    type: types.DELETE_LANDER,
    payload
  }),

  saveLanderAsync: (payload, meta) => ({
    type: types.SAVE_LANDER_ASYNC,
    payload,
    meta
  }),

  fetchLandersAsync: () => ({
    type: types.FETCH_LANDERS_ASYNC
  }),

  deleteLanderAsync: (payload) => ({
    type: types.DELETE_LANDER_ASYNC,
    payload
  })
};
