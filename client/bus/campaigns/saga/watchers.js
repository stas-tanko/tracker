import { takeEvery, all, call } from 'redux-saga/effects';

import { types } from '../types';
import { saveCampaign, fetchCampaigns, deleteCampaign } from './workers';

function* watchSaveCampaign() {
  yield takeEvery(types.SAVE_CAMPAIGN_ASYNC, saveCampaign);
}

function* watchFetchCampaigns() {
  yield takeEvery(types.FETCH_CAMPAIGNS_ASYNC, fetchCampaigns);
}

function* watchDeleteCampaign() {
  yield takeEvery(types.DELETE_CAMPAIGN_ASYNC, deleteCampaign);
}

export function* watchCampaigns() {
  yield all([
    call(watchSaveCampaign),
    call(watchFetchCampaigns),
    call(watchDeleteCampaign)
  ]);
}
