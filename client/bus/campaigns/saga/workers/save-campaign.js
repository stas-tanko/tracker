import { apply, put } from 'redux-saga/effects';

import { campaignsApi } from '../../../../api/campaigns';
import { campaignsActions } from '../../../campaigns/actions';

export function* saveCampaign({ payload, meta }) {
  try {
    if (payload._id) {
      const { data } = yield apply(campaignsApi, campaignsApi.update, [payload]);
      yield put(campaignsActions.updateCampaign(data));
    } else {
      const { data } = yield apply(campaignsApi, campaignsApi.create, [payload]);
      yield put(campaignsActions.createCampaign(data));
    }
    yield apply(meta, meta.onSuccess);
  } catch (error) {
    console.log(error);
  }
};
