import { apply, put } from 'redux-saga/effects';

import { campaignsApi } from '../../../../api/campaigns';
import { campaignsActions } from '../../../campaigns/actions';

export function* deleteCampaign({ payload: id }) {
  try {
    yield apply(campaignsApi, campaignsApi.delete, [id]);
    yield put(campaignsActions.deleteCampaign(id));
  } catch (error) {
    console.log(error);
  }
};
