import { apply, put } from 'redux-saga/effects';

import { campaignsApi } from '../../../../api/campaigns';
import { campaignsActions } from '../../../campaigns/actions';

export function* fetchCampaigns() {
  try {
    const { data } = yield apply(campaignsApi, campaignsApi.fetch);
    yield put(campaignsActions.fetchCampaigns(data));
  } catch (error) {
    console.log(error);
  }
};
