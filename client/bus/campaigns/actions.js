import { types } from './types';

export const campaignsActions = {
  fetchCampaigns: (payload) => ({
    type: types.FETCH_CAMPAIGNS,
    payload
  }),

  createCampaign: (payload) => ({
    type: types.CREATE_CAMPAIGN,
    payload
  }),

  updateCampaign: (payload) => ({
    type: types.UPDATE_CAMPAIGN,
    payload
  }),

  deleteCampaign: (payload) => ({
    type: types.DELETE_CAMPAIGN,
    payload
  }),

  saveCampaignAsync: (payload, meta) => ({
    type: types.SAVE_CAMPAIGN_ASYNC,
    payload,
    meta
  }),

  fetchCampaignsAsync: () => ({
    type: types.FETCH_CAMPAIGNS_ASYNC
  }),

  deleteCampaignAsync: (payload) => ({
    type: types.DELETE_CAMPAIGN_ASYNC,
    payload
  })
};
