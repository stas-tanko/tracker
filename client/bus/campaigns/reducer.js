import { types } from './types';

const initialState = {
  items: [],
};

export const campaignsReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case types.CREATE_CAMPAIGN:
      return { items: [...state.items, payload] };

    case types.UPDATE_CAMPAIGN:
      return {
        items: [
          ...state.items.filter((item) => item._id !== payload._id),
          payload
        ]
      };

    case types.FETCH_CAMPAIGNS:
      return { items: payload };

    case types.DELETE_CAMPAIGN:
      return {
        items: state.items.filter((item) => item._id !== payload)
      };

    default:
      return state;
  }
};
